var app = angular.module('md-datetimepicker', []);

app.directive('datetimepicker', [ function (){
  var uniqueId = 1;
  return {
  	restrict: 'E',
  	require: ['ngModel'],
  	//transclude: true,
  	replace: true,
  	template: '<input type="text"></input>',
  	link: function($scope, el, attr, ngModel){ //$scope eh utilizado as vezes ao invez de "scope" para diferenciar o scope de cima do de baixo
  		var idOfElement = attr.id !== undefined ? attr.id : "__datetimepicker" + uniqueId + "__";
		var dtp = document.getElementById(idOfElement);
		dtp = angular.element('#' + idOfElement);

		var minDt = attr.mindate && attr.mindate.toLowerCase() === 'yesterday' ? '-1970/01/02' : attr.mindate;
		var maxDt = attr.maxdate && attr.maxdate.toLowerCase() === 'tomorrow' ? '+1970/01/02' : attr.maxdate;

		if (dtp){
			var params = {
		    	datepicker: attr.datepicker ? attr.datepicker.toLowerCase() === 'true' : true
		    	,value: ngModel.$viewValue
		    	,format: attr.format
		    	,step: attr.step
		    	,minDate: minDt
		    	,maxDate: maxDt
		    	,onSelectDate: onSelectDateFn
	    	}
	    }

		function onSelectDateFn(currentTime, dtp){
    		$scope.$apply(function(){
				dtp.datetimepicker('setOptions', {value: currentTime});
			});
    	}


	    if(attr.lang){
	    	$.datetimepicker.setLocale(attr.lang != 'en' ? attr.lang : 'en');
	    }
	    dtp.datetimepicker(params);
		uniqueId++;
  	}
  }
}]);

//QUESTIOidOfElementNTOS DO ESTUDO:
//1. Qual a diferença entre angular.element elementParam, e elementParam.find();